## Endpoints

This API exposes 4 endpoints.


### Usage

Start server with
```bash
go run main.go
```
or you can use Docker
```bash
docker build -t web . && docker run -it -p 3000:3000 web:latest
```

You can try this api online on heroku replacing localhost:3000 with [https://insurance-company.herokuapp.com](https://insurance-company.herokuapp.com)

### PING: `HEAD /ping`
```bash
curl -I "http://localhost:3000/ping"
```

### Get user by name or id: `GET /user`

```bash
curl "localhost:3000/user?id=4a0573eb-56d0-45d5-ab36-bebf33c5eb36" -H 'email:inesblankenship@quotezart.com'
```
```bash
curl "localhost:3000/user?name=Ines" -H 'email:inesblankenship@quotezart.com'
```

### Get policies by user: `GET /user/policy/:name`

```bash
curl "localhost:3000/user/policy/Ines" -H 'email:barnettblankenship@quotezart.com'
```

### Get policy by policyID: `GET /policy/:number`

```bash
curl "localhost:3000/policy/0df3bcef-7a14-4dd7-a42d-fa209d0d5804" -H 'email:manningblankenship@quotezart.com'
```

