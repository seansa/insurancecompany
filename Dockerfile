FROM golang
WORKDIR $GOPATH/src/bitbucket.com/seansa/insuranceCompany
COPY . .
RUN go get -d -v ./...
RUN CGO_ENABLED=0 GOOS=linux go install -v ./...

FROM alpine:latest
WORKDIR /root/
COPY --from=0 /go/bin/api .
CMD ["./api"]