#! /usr/bin/env bash

# usage: sh test.sh [-c] [-v] [-m] [-t <sepcific_test>]
# -c produces coverage report
# -t <specific_test> runs go test with the option "-run specific_test"

# parses for options -v and -t
function opts() {
	while getopts "t:v" opt; do
		case $opt in
			t) specific_test="-run $OPTARG"; ;;
			v) verbose="-v"; ;;
		esac
	done
}

opts $@
export DATA_DIR=$(pwd)/data

cd src/api
for dir in $(find . -name "*_test.go" -not -path "./vendor/*" |sed 's#\(.*\)/.*#\1#' | sort -u)
do
    cd $dir
    go test $verbose $specific_test
    test_result="$?"
    cd -
    if [ $test_result != 0 ]; then
        break
    fi
done

exit $((test_result))