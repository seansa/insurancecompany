package app

import (
	"net/http"

	"bitbucket.com/seansa/insuranceCompany/src/api/controllers"
	"bitbucket.com/seansa/insuranceCompany/src/api/handlers"
	"github.com/gin-gonic/gin"
)

func urlToControllers() {
	status()
	router.GET("/user", handlers.IsUserOrAdmin, controllers.GetUser)
	router.GET("/user/policy/:name", handlers.IsUserOrAdmin, controllers.GetPoliciesByUserName)
	router.GET("/policy/:number", handlers.IsAdmin, controllers.GetPoliciesByPolicyNumber)
}

func status() {
	router.HEAD("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"status": "pong"})
	})
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"status": "pong"})
	})
}
