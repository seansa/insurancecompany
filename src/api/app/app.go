package app

import (
	"fmt"
	"os"

	"bitbucket.com/seansa/insuranceCompany/src/api/config"
	"github.com/gin-gonic/gin"
)

var router *gin.Engine

func Start() {
	setupRouter()
	urlToControllers()
	port := os.Getenv("PORT")
	if port == "" && !config.IsProduction() {
		port = "3000"
	}
	fmt.Println(fmt.Sprintf("run server at: %v", port))
	router.Run("0.0.0.0:" + port)
}

func setupRouter() {
	router = gin.Default()
	router.RedirectFixedPath = false
	router.RedirectTrailingSlash = false
	router.Use(gin.Logger())
}
