package models

type Error struct {
	Code  int         `json:"code"`
	Cause interface{} `json:"cause"`
	Error string      `json:"error,omitempty"`
}
