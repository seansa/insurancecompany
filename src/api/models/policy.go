package models

import "time"

type Policy struct {
	ID                 string    `json:"id"`
	AmountInsured      float64   `json:"amountInsured"`
	Email              string    `json:"email"`
	InceptionDate      time.Time `json:"inceptionDate"`
	InstallmentPayment bool      `json:"installmentPayment"`
	ClientID           string    `json:"clientId"`
}

type Policies struct {
	Policies []Policy `json:"policies"`
}
