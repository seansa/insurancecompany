package services

import (
	"bitbucket.com/seansa/insuranceCompany/src/api/dao"
	"bitbucket.com/seansa/insuranceCompany/src/api/models"
)

func GetUserByID(id string) (*models.User, *models.Error) {
	user, err := dao.GetUserByID(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func GetUserByName(name string) (*models.User, *models.Error) {
	user, err := dao.GetUserByName(name)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func GetUserByEmail(email string) (*models.User, *models.Error) {
	user, err := dao.GetUserByEmail(email)
	if err != nil {
		return nil, err
	}
	return user, nil
}
