package services

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetpPoliciesByName(t *testing.T) {
	assert := assert.New(t)

	t.Run("GetpPoliciesByName OK", func(t *testing.T) {
		policy, err := GetpPoliciesByName("Ines")
		assert.Nil(err)
		assert.Len(policy.Policies, 193)
	})

	t.Run("GetpPoliciesByName FAIL", func(t *testing.T) {
		policy, err := GetpPoliciesByName("Sebastian")
		assert.NotNil(err)
		assert.Nil(policy)
	})
}

func TestGetpPoliciesByNumber(t *testing.T) {
	assert := assert.New(t)

	t.Run("GetpPoliciesByNumber OK", func(t *testing.T) {
		user, err := GetpPoliciesByNumber("0df3bcef-7a14-4dd7-a42d-fa209d0d5804")
		assert.Nil(err)
		assert.NotNil(user)
		assert.Equal("a0ece5db-cd14-4f21-812f-966633e7be86", user.ID)
		assert.Equal("Britney", user.Name)
		assert.Equal("britneyblankenship@quotezart.com", user.Email)
		assert.Equal("admin", user.Role)
	})

	t.Run("GetpPoliciesByNumber FAIL", func(t *testing.T) {
		user, err := GetpPoliciesByNumber("00000000-0000-0000-0000-fa209d0d5804")
		assert.NotNil(err)
		assert.Nil(user)
	})

	t.Run("GetpPoliciesByNumber FAIL", func(t *testing.T) {
		user, err := GetpPoliciesByNumber("hi")
		assert.NotNil(err)
		assert.Nil(user)
	})
}
