package services

import (
	"bitbucket.com/seansa/insuranceCompany/src/api/dao"
	"bitbucket.com/seansa/insuranceCompany/src/api/models"
)

func GetpPoliciesByName(name string) (*models.Policies, *models.Error) {
	policies, err := dao.GetPoliciesByName(name)
	if err != nil {
		return nil, err
	}
	return policies, nil
}

func GetpPoliciesByNumber(number string) (*models.User, *models.Error) {
	user, err := dao.GetUserByPolicyNumber(number)
	if err != nil {
		return nil, err
	}
	return user, nil
}
