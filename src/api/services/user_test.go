package services

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetUserByID(t *testing.T) {
	assert := assert.New(t)

	t.Run("GetUserByID OK", func(t *testing.T) {
		user, err := GetUserByID("a0ece5db-cd14-4f21-812f-966633e7be86")
		assert.Nil(err)
		assert.Equal("Britney", user.Name)
	})

	t.Run("GetUserByID FAIL", func(t *testing.T) {
		user, err := GetUserByID("a0ece5db-1111-1111-1111-11111111")
		assert.NotNil(err)
		assert.Nil(user)
	})
}

func TestGetUserByName(t *testing.T) {
	assert := assert.New(t)

	t.Run("GetUserByName OK", func(t *testing.T) {
		user, err := GetUserByName("Britney")
		assert.Nil(err)
		assert.Equal("a0ece5db-cd14-4f21-812f-966633e7be86", user.ID)
	})

	t.Run("GetUserByName FAIL", func(t *testing.T) {
		user, err := GetUserByName("Sebastian")
		assert.NotNil(err)
		assert.Nil(user)
	})
}

func TestGetUserByEmail(t *testing.T) {
	assert := assert.New(t)

	t.Run("GetUserByEmail OK", func(t *testing.T) {
		user, err := GetUserByEmail("britneyblankenship@quotezart.com")
		assert.Nil(err)
		assert.Equal("Britney", user.Name)
	})

	t.Run("GetUserByEmail FAIL", func(t *testing.T) {
		user, err := GetUserByEmail("a0ece5db-3333-4444-2222-11111111")
		assert.NotNil(err)
		assert.Nil(user)
	})
}
