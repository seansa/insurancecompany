package validators

import (
	"bitbucket.com/seansa/insuranceCompany/src/api/models"
	"bitbucket.com/seansa/insuranceCompany/src/api/utils"
)

func ValidateGet(id, name string) *models.Error {
	if id == "" && name == "" {
		return utils.BadRequestError("Invalid request User or ID required")
	}
	if len(id) < 36 && name == "" {
		return utils.BadRequestError("Invalid User ID")
	}
	if len(id) == 36 || name != "" {
		return nil
	}
	return utils.BadRequestError("Invalid request User or ID required")
}
