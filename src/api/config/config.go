package config

import "os"

const (
	Name  = "name"
	ID    = "id"
	Email = "email"
	Admin = "admin"
	User  = "user"
)

func IsProduction() bool {
	if env := os.Getenv("GO_ENVIRONMENT"); env == "production" {
		return true
	}
	return false
}
