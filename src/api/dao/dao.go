package dao

import (
	"fmt"

	"bitbucket.com/seansa/insuranceCompany/src/api/config"
	"bitbucket.com/seansa/insuranceCompany/src/api/models"
	"bitbucket.com/seansa/insuranceCompany/src/api/utils"
)

func GetUserByID(id string) (*models.User, *models.Error) {
	data, err := getUserJSON()
	if err != nil {
		return nil, err
	}
	if u := filterData(id, config.ID, *data); u != nil {
		return u, nil
	}
	return nil, utils.NotFoundError(fmt.Sprintf("Not found User with id: %v", id))
}

func GetUserByName(name string) (*models.User, *models.Error) {
	data, err := getUserJSON()
	if err != nil {
		return nil, err
	}
	if u := filterData(name, config.Name, *data); u != nil {
		return u, nil
	}
	return nil, utils.NotFoundError(fmt.Sprintf("Not found User with name: %v", name))
}

func GetUserByEmail(email string) (*models.User, *models.Error) {
	data, err := getUserJSON()
	if err != nil {
		return nil, err
	}
	if u := filterData(email, config.Email, *data); u != nil {
		return u, nil
	}
	return nil, utils.NotFoundError(fmt.Sprintf("Not found User with name: %v", email))
}

func filterData(key string, tpe string, data models.Clients) *models.User {
	for _, u := range data.Clients {
		switch tpe {
		case config.Name:
			if u.Name == key {
				return &u
			}
		case config.ID:
			if u.ID == key {
				return &u
			}
		case config.Email:
			if u.Email == key {
				return &u
			}
		}
	}
	return nil
}

func GetPoliciesByName(name string) (*models.Policies, *models.Error) {
	var err *models.Error
	var data *models.Policies
	data, err = getPoliciesJSON()
	if err != nil {
		return nil, err
	}
	var user *models.User
	user, err = GetUserByName(name)
	if err != nil {
		return nil, err
	}
	result := new(models.Policies)

	for _, p := range data.Policies {
		if p.Email == user.Email {
			result.Policies = append(result.Policies, p)
		}
	}
	if len(result.Policies) == 0 {
		return nil, utils.NotFoundError(fmt.Sprintf("Not found policies for User with name: %v", name))
	}
	return result, nil
}

func GetUserByPolicyNumber(number string) (*models.User, *models.Error) {
	var err *models.Error
	var data *models.Policies
	data, err = getPoliciesJSON()
	if err != nil {
		return nil, err
	}
	var policy *models.Policy
	for _, p := range data.Policies {
		if p.ID == number {
			policy = &p
			break
		}
	}
	if policy == nil {
		return nil, utils.NotFoundError(fmt.Sprintf("Not found policies with id: %v", number))
	}
	var user *models.User
	user, err = GetUserByID(policy.ClientID)
	if err != nil {
		return nil, err
	}
	return user, nil
}
