package utils

import (
	"net/http"

	"bitbucket.com/seansa/insuranceCompany/src/api/models"
)

func BadRequestError(cause string) *models.Error {
	return &models.Error{Code: http.StatusBadRequest, Cause: cause, Error: ""}
}

func NotFoundError(cause string) *models.Error {
	return &models.Error{Code: http.StatusNotFound, Cause: cause, Error: ""}
}

func InternalServerError(cause string, err error) *models.Error {
	return &models.Error{Code: http.StatusInternalServerError, Cause: cause, Error: err.Error()}
}

func UnauthorizedError(cause string) *models.Error {
	return &models.Error{Code: http.StatusUnauthorized, Cause: cause, Error: ""}
}
