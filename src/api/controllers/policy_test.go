package controllers

import (
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.com/seansa/insuranceCompany/src/api/handlers"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var r *gin.Engine

func init() {
	r = setupRouter(gin.New())
}

func setupRouter(engine *gin.Engine) *gin.Engine {
	gin.SetMode(gin.TestMode)
	r := engine
	r.RedirectFixedPath = false
	r.RedirectTrailingSlash = false
	r.GET("/user", handlers.IsUserOrAdmin, GetUser)
	r.GET("/user/policy/:name", handlers.IsUserOrAdmin, GetPoliciesByUserName)
	r.GET("/policy/:number", handlers.IsAdmin, GetPoliciesByPolicyNumber)
	return r
}
func TestGetPoliciesByUserName(t *testing.T) {
	assert := assert.New(t)
	t.Run("TestGetPoliciesByUserName OK", func(t *testing.T) {
		res := performRequest("GET", "/user/policy/Ines", "", "barnettblankenship@quotezart.com", true, r)
		assert.Equal(200, res.Code)
	})
	t.Run("TestGetPoliciesByUserName FAIL", func(t *testing.T) {
		res := performRequest("GET", "/user/policy/Ines", "", "barnettblankenship@quotezart.com", false, r)
		assert.Equal(401, res.Code)
	})
}

func performRequest(method, target, body, email string, role bool, engine *gin.Engine) *httptest.ResponseRecorder {
	payload := strings.NewReader(body)
	req := httptest.NewRequest(method, target, payload)
	if role {
		req.Header.Add("email", email)
	}
	res := httptest.NewRecorder()
	engine.ServeHTTP(res, req)
	return res
}
