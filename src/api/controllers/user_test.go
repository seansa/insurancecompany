package controllers

import (
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func init() {
	r = setupRouter(gin.New())
}

func TestGetUser(t *testing.T) {
	assert := assert.New(t)
	t.Run("TestGetPoliciesByUserId OK", func(t *testing.T) {
		res := performRequest("GET", "/user?id=4a0573eb-56d0-45d5-ab36-bebf33c5eb36", "", "barnettblankenship@quotezart.com", true, r)
		assert.Equal(200, res.Code)
	})
	t.Run("TestGetPoliciesByUserId FAIL 401", func(t *testing.T) {
		res := performRequest("GET", "/user?id=4a0573eb-56d0-45d5-ab36-bebf33c5eb36", "", "barnettblankenship@quotezart.com", false, r)
		assert.Equal(401, res.Code)
	})

	t.Run("TestGetPoliciesByUserId FAIL 400", func(t *testing.T) {
		res := performRequest("GET", "/user", "", "barnettblankenship@quotezart.com", false, r)
		assert.Equal(401, res.Code)
	})

	t.Run("TestGetPoliciesByUserId FAIL 404", func(t *testing.T) {
		res := performRequest("GET", "/user?id=1234", "", "sebastian@quotezart.com", true, r)
		assert.Equal(404, res.Code)
	})

	t.Run("TestGetPoliciesByUserName OK", func(t *testing.T) {
		res := performRequest("GET", "/user?name=Ines", "", "barnettblankenship@quotezart.com", true, r)
		assert.Equal(200, res.Code)
	})
	t.Run("TestGetPoliciesByUserName FAIL 401", func(t *testing.T) {
		res := performRequest("GET", "/user?name=Ines", "", "barnettblankenship@quotezart.com", false, r)
		assert.Equal(401, res.Code)
	})

	t.Run("TestGetPoliciesByUserName FAIL 400", func(t *testing.T) {
		res := performRequest("GET", "/user", "", "barnettblankenship@quotezart.com", false, r)
		assert.Equal(401, res.Code)
	})

	t.Run("TestGetPoliciesByUserName FAIL 404", func(t *testing.T) {
		res := performRequest("GET", "/user?name=312312", "", "sebastian@quotezart.com", true, r)
		assert.Equal(404, res.Code)
	})

}
