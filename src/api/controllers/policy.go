package controllers

import (
	"net/http"

	"bitbucket.com/seansa/insuranceCompany/src/api/services"
	"bitbucket.com/seansa/insuranceCompany/src/api/utils"
	"github.com/gin-gonic/gin"
)

//GetPoliciesByUserName policies by user name
func GetPoliciesByUserName(c *gin.Context) {
	name := c.Param("name")
	if name == "" {
		err := utils.BadRequestError("User Name required")
		c.JSON(err.Code, err)
		return
	}
	policies, err := services.GetpPoliciesByName(name)
	if err != nil {
		c.JSON(err.Code, err)
		return
	}
	c.JSON(http.StatusOK, policies)
}

//GetPoliciesByPolicyNumber Get policy by policy number
func GetPoliciesByPolicyNumber(c *gin.Context) {
	number := c.Param("number")
	if number == "" {
		err := utils.BadRequestError("Policy number required")
		c.JSON(err.Code, err)
		return
	}
	user, err := services.GetpPoliciesByNumber(number)
	if err != nil {
		c.JSON(err.Code, err)
		return
	}
	c.JSON(http.StatusOK, user)
}
