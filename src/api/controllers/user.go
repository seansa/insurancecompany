package controllers

import (
	"net/http"

	"bitbucket.com/seansa/insuranceCompany/src/api/models"
	"bitbucket.com/seansa/insuranceCompany/src/api/validators"

	"bitbucket.com/seansa/insuranceCompany/src/api/services"

	"github.com/gin-gonic/gin"
)

//GetUser Get user data filtered by user id or name sending queyparams
func GetUser(c *gin.Context) {
	id := c.Query("id")
	name := c.Query("name")
	if err := validators.ValidateGet(id, name); err != nil {
		c.JSON(err.Code, err)
		return
	}
	var user *models.User
	var err *models.Error
	if id != "" {
		user, err = services.GetUserByID(id)
		if err != nil {
			c.JSON(err.Code, err)
			return
		}
	}
	if name != "" {
		user, err = services.GetUserByName(name)
		if err != nil {
			c.JSON(err.Code, err)
			return
		}
	}
	c.JSON(http.StatusOK, user)
}
