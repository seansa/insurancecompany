package handlers

import (
	"bitbucket.com/seansa/insuranceCompany/src/api/config"
	"bitbucket.com/seansa/insuranceCompany/src/api/services"
	"bitbucket.com/seansa/insuranceCompany/src/api/utils"
	"github.com/gin-gonic/gin"
)

func IsAdmin(c *gin.Context) {
	header := c.GetHeader("email")
	if header == "" {
		e := utils.UnauthorizedError("Unauthorized")
		c.JSON(e.Code, e)
		c.Abort()
		return
	}
	user, err := services.GetUserByEmail(header)
	if err != nil {
		c.JSON(err.Code, err)
		c.Abort()
		return
	}
	if user.Role != config.Admin {
		e := utils.UnauthorizedError("Unauthorized")
		c.JSON(e.Code, e)
		c.Abort()
		return
	}
	c.Next()
}

func IsUserOrAdmin(c *gin.Context) {
	header := c.GetHeader("email")
	if header == "" {
		e := utils.UnauthorizedError("Unauthorized")
		c.JSON(e.Code, e)
		c.Abort()
		return
	}
	user, err := services.GetUserByEmail(header)
	if err != nil {
		c.JSON(err.Code, err)
		c.Abort()
		return
	}
	if user.Role != config.Admin {
		if user.Role != config.User {
			e := utils.UnauthorizedError("Unauthorized")
			c.JSON(e.Code, e)
			c.Abort()
			return
		}
	}
	c.Next()
}
